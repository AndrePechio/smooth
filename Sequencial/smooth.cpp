#include <opencv/cv.h>
#include <opencv/highgui.h>
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <time.h>
#include "opencv2/core/core_c.h"
#include "opencv2/core/core.hpp"
#include "opencv2/flann/miniflann.hpp"
#include "opencv2/imgproc/imgproc_c.h"
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/video/video.hpp"
#include "opencv2/features2d/features2d.hpp"
#include "opencv2/objdetect/objdetect.hpp"
#include "opencv2/calib3d/calib3d.hpp"
#include "opencv2/ml/ml.hpp"
#include "opencv2/highgui/highgui_c.h"
#include "opencv2/highgui/highgui.hpp"

using namespace std;
using namespace cv;

#define MAXVAL 256
#define KERNELSIZE 5

#define RODA 10

//g++ `pkg-config --cflags opencv` smooth.cpp `pkg-config --libs opencv` -o T

unsigned char* addPadding(unsigned char* img, int size, float value, int h, int w);
void applyMeanFilter(unsigned char*img, unsigned char *imgres, int h, int w, int kernelsize);

int main(int argc, char **argv){
	
	
	Mat in, RGB[3];
	int h,w,t;
	unsigned char *dataV, *data;
	clock_t inicio, fim, total;
	long double seconds, gray;
	long double tudo = 0;
		
	//presença de arg define se a img vai ser aberta em GRAY ou COLOR
	if(argc > 2){
		gray = 1;
		in = imread(argv[1],CV_LOAD_IMAGE_GRAYSCALE);
		printf("gray img\n");
	}
	else{
		gray = 0;
		in = imread(argv[1],CV_LOAD_IMAGE_COLOR);
		printf("color img\n");
		
	
	}
	
	h = in.rows;
	w = in.cols;
	
	t = 0;
	
	while(t < RODA){
		
		inicio = clock();
			
		if(!gray){
			
	
			// Conta o tempo decorrido em SEGUNDOS
			inicio = clock();
			
			split(in,RGB);
			
			//Calculate R
			
			dataV = (unsigned char*) RGB[0].data;
			data = (unsigned char*) RGB[0].data;
			
			applyMeanFilter(dataV,data,h,w,KERNELSIZE);
			
			//Calculate G
			
			dataV = (unsigned char*) RGB[1].data;
			data = (unsigned char*) RGB[1].data;
			
			applyMeanFilter(dataV,data,h,w,KERNELSIZE);
			
			//Calculate B
			
			dataV = (unsigned char*) RGB[2].data;
			data = (unsigned char*) RGB[2].data;
			
			applyMeanFilter(dataV,data,h,w,KERNELSIZE);
	
			//Marca o final da execução do algoritmo Smooth
		
		}
		else{
						
			dataV = (unsigned char*) in.data;
			data = (unsigned char*) in.data;
			
			applyMeanFilter(dataV,data,h,w,KERNELSIZE);
			
		}
		
		fim = clock();
			
		total = (fim - inicio);
		
		seconds = (((long double)total) / CLOCKS_PER_SEC);
		tudo += seconds;
		//printf("\nInicio (em Segundos):                            \t\t\t%Lf", (((long double)inicio) / CLOCKS_PER_SEC));
		//printf("\nFim (em Segundos):                                \t\t\t%Lf", (((long double)fim) / CLOCKS_PER_SEC));
		//Mostra o tempo que demorou
		printf("%Lf",seconds);
		printf("\n");

		t++;
	}
	//calcula a media de tempo das 10 execs
	tudo = tudo/RODA;
	
	printf("Media de 10 exec: %Lf \n",tudo);
	
	if(!gray)
		merge(RGB,3,in);

	imwrite("smoothed.jpg",in);
		
	//cvWaitKey(0);

	in.release();
	
	return 0;
	
}

void applyMeanFilter(unsigned char*img, unsigned char *imgres, int h, int w, int kernelsize){
	
	int i,j,l,k;
	unsigned char* padded;
	float num;
	int paddingsize;
	float *kernel;
	int total;
	
	total = kernelsize*kernelsize;
	
	paddingsize = kernelsize/2;
	kernel = (float*)malloc(total*sizeof(float));
	
	for(i = 0; i < total; i++){
		kernel[i] = 1.0/total;
	}

	padded = addPadding(img,paddingsize,MAXVAL/2,h,w);
	
	for(i = 0; i < (h + (paddingsize*2)); i++){
		for(j = 0; j < (w + (paddingsize*2)); j++){
			
			if( i < h && j < w){
				num = 0;
				for(l = 0; l < kernelsize; l++){
					for(k = 0; k < kernelsize; k++){
						num = num + padded[((i+l)*(w)+(j + k))] * kernel[(l*(kernelsize)) + k];
					}
				}
				imgres[(i*(w)) + j] = (unsigned char)num;
			}
		
		}
	}
	free(padded);
	free(kernel);
}

unsigned char* addPadding(unsigned char* img, int size, float value, int h, int w){

	int i,j;
	unsigned char* padded;
	
	padded = (unsigned char*)malloc(((h+4)*(w+4))*sizeof(unsigned char));
	if(padded == NULL)
		exit(1);

	
	for(i = 0; i < (h + (size*2)); i++){
		for(j = 0; j < (w + (size*2)); j++){
			
			if(i < size || j < size || i >= h || j >= w)
				padded[i*(w) + j] = value;
			else
				padded[i*(w) + j] = img[i*(w) + j];			
		}
	}
	
	return padded;

}
