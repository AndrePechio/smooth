#include <omp.h>
#include <iostream>
#include <opencv/cv.h>
#include <opencv/highgui.h>
#include <math.h>
#include "mpi.h"
#include <string.h>
#include <time.h>
#include <stdlib.h>
#include <stdio.h>
#include <sys/time.h>
//#include "opencv2/core/core_c.h"
//#include "opencv2/core/core.hpp"
//#include "opencv2/highgui/highgui.hpp"

using namespace std;
using namespace cv;

#define MAXVAL 256
#define OUTER_THREAD_NUM 4
#define INNER_THREAD_NUM 4

#define RODAR 3

#define T_R 1
#define T_G 2
#define T_B 3
#define MAIN 0

struct timeval fim;

//clock_t fim, total;
float tudo = 0;

unsigned char* addPadding(unsigned char* img, int size, float value, int h, int w);
void applyMeanFilterParalell(unsigned char*img, unsigned char *imgres, int h, int w,int,int, int kernelsize);
void finishTime(struct timeval);

int main(int argc, char** argv){
		
	int t;
	int h, w, ctid, pedaco, rank, send = 1, i ,isColor, receive = 1, size;
	IplImage *aux, *RGB[3];
	unsigned char *dataV, *data, *dataAux, *dadosImg, *dadosNovos, *datas[3];
	int gray;
	//clock_t inicio;
	struct timeval inicio;
	
	int KERNELSIZE = 5;
	
	MPI_Init(&argc, &argv);
	MPI_Comm_size(MPI_COMM_WORLD, &size);
	MPI_Status status;
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	
	if(argc > 2)
		gray = 1;
	else
		gray = 0;
	
	if(rank == 0){
		if(gray){
			aux = cvLoadImage(argv[1],CV_LOAD_IMAGE_GRAYSCALE);
	
			
		}
		else{
			aux = cvLoadImage(argv[1],CV_LOAD_IMAGE_COLOR);
		}
		
				
		if(aux == NULL)
			exit(1);
		
		h = aux->height;
		w = aux->width;
		
		if(!gray){
			for(i = 0; i < 3; i++){
				RGB[i] = cvCreateImage(cvSize(w,h),aux->depth,1);
				if(RGB[i] == NULL){
					printf("FATAL ERROR\n");
					exit(1);
				}
			}
		}
		
		
	}
	
	t = 0;
	
	while(t < RODAR){
		
		if(rank == 0){
		
			gettimeofday(&inicio,0);
			
			if(!gray){
				
				isColor = 1;
				
				cvSplit(aux,RGB[0],RGB[1],RGB[2],NULL);
								
				for(i = 1; i < 4; i++){
	             	MPI_Send(&h, 1, MPI_INT, i, send, MPI_COMM_WORLD);
	                MPI_Send(&w, 1, MPI_INT, i, send, MPI_COMM_WORLD);
				}
				
				for(i = 0; i < 3; i++){
					datas[i] = (unsigned char*)RGB[i]->imageData;
					if(t == 0)
						datas[i] = addPadding(datas[i],KERNELSIZE/2,0,h,w);
				}
				
				for( i = 0; i < h + 4 ; i++){
					
					MPI_Send(&datas[0][i*(w+4)], (w + 4), MPI_UNSIGNED_CHAR, T_R,  send, MPI_COMM_WORLD);
	            	MPI_Send(&datas[1][i*(w+4)], (w + 4), MPI_UNSIGNED_CHAR, T_G,  send, MPI_COMM_WORLD);
	            	MPI_Send(&datas[2][i*(w+4)], (w + 4), MPI_UNSIGNED_CHAR, T_B,  send, MPI_COMM_WORLD);	
					
				}
				
				for( i = 0; i < h + 4; i++){
					
					MPI_Recv(&datas[0][i*(w+4)], (w + 4), MPI_UNSIGNED_CHAR, T_R,  receive, MPI_COMM_WORLD,&status);
	            	MPI_Recv(&datas[1][i*(w+4)], (w + 4), MPI_UNSIGNED_CHAR, T_G,  receive, MPI_COMM_WORLD,&status);
	            	MPI_Recv(&datas[2][i*(w+4)], (w + 4), MPI_UNSIGNED_CHAR, T_B,  receive, MPI_COMM_WORLD,&status);	
				}
				
				RGB[1]->imageData = (char*)datas[1];
				RGB[0]->imageData = (char*)datas[0];
				RGB[2]->imageData = (char*)datas[2];
				if(t == RODAR - 1)
					cvMerge(RGB[0],RGB[1],RGB[2],0,aux);
				
					
				imwrite("smoothed.jpg",in);
				//cvWaitKey(0);
				
	
				
			}else{ // GRAY
				
				dataV = (unsigned char*)aux->imageData;
				data = (unsigned char*)aux->imageData;
				
				if(t == 0)
					 dataAux = (unsigned char*)malloc((h + 4 )*(w + 4)*sizeof(unsigned char));
				
				isColor = 0;
				
				dataV = addPadding(dataV,KERNELSIZE/2,0,h,w);
				
				#pragma omp parallel num_threads(4) private(ctid,pedaco) shared(dataV)
				{
					ctid = omp_get_thread_num();
					pedaco = h/INNER_THREAD_NUM;
					if(t < RODAR - 1)
						applyMeanFilterParalell(dataV,dataAux,ctid*pedaco,(ctid+1)*pedaco,0,w,KERNELSIZE);
					else{
						applyMeanFilterParalell(dataV,data,ctid*pedaco,(ctid+1)*pedaco,0,w,KERNELSIZE);
					}
				}
				
				if(t == RODAR - 1){}
					free(dataAux);
				
			}
			
			if(t == RODAR - 1){
				
				//cvShowImage("original",aux);
				
				if(isColor){
					
					for(i=0;i<3;i++){
						cvReleaseImage(&RGB[i]);
						free(datas[i]);
					}
				}
				//cvWaitKey(0);
				cvReleaseImage(&aux);
			}
			
			finishTime(inicio);
			
			
		}
		
		if(rank > 0 && !gray){
			
			MPI_Recv(&h, 1, MPI_INT, MAIN, receive , MPI_COMM_WORLD, &status);
			MPI_Recv(&w, 1, MPI_INT, MAIN, receive, MPI_COMM_WORLD, &status);
			
			if(t == 0){
				
				dadosImg = (unsigned char*)malloc((h + 4 )*(w + 4)*sizeof(unsigned char));
				dadosNovos = (unsigned char*)malloc((h + 4 )*(w + 4)*sizeof(unsigned char));
			}
			
			for(i = 0; i < h + 4 ; i++){
				
				MPI_Recv(&dadosImg[i*(w+4)], w + 4 , MPI_UNSIGNED_CHAR, MAIN, receive, MPI_COMM_WORLD, &status);
			}
				
				
			#pragma omp parallel num_threads(4) private(ctid,pedaco)
			{
				ctid = omp_get_thread_num();
				pedaco = h/INNER_THREAD_NUM;
				applyMeanFilterParalell(dadosImg,dadosNovos,ctid*pedaco,(ctid+1)*pedaco,0,w,KERNELSIZE);
				
			}
			
			
			for(i = 0; i < h+4; i++)
				MPI_Send(&dadosNovos[i*(w+4)], (w + 4), MPI_UNSIGNED_CHAR, MAIN,  send, MPI_COMM_WORLD);
						
			if(t == RODAR - 1){
				free(dadosNovos);
				free(dadosImg);
			}
		}
		else if(rank > 0 && gray){
			MPI_Finalize();
			return 0;
		}
		
		t++;
			
	}
	
	if(rank == 0){
		printf("Media: %f\n",tudo/RODAR);
	}

	
	MPI_Finalize();
	
	return 0;
}
	
	


void applyMeanFilterParalell(unsigned char*img, unsigned char *imgres, int h0, int h, int w0, int w, int kernelsize){
	
	int i,j,l,k;
	float num;
	float *kernel;
	int total;
	
	total = kernelsize*kernelsize;
	
	kernel = (float*)malloc(total*sizeof(float));
	
	for(i = 0; i < total; i++){
		kernel[i] = 1.0/total;
	}

	for(i = h0; i < h; i++){
		for(j = w0; j < w; j++){
			
			if( i < h && j < w){
				num = 0;
				for(l = 0; l < kernelsize; l++){
					for(k = 0; k < kernelsize; k++){
						num = num + img[ (i+l)*(w) + (j + k)]*kernel[l*(kernelsize) + k];
					}
				}
				imgres[i*(w) + j] = (unsigned char)num;
			}
		
		}
	}
	free(kernel);
}


void finishTime(struct timeval inicio){
	
	//long double seconds;
	
	gettimeofday(&fim,0);
	float total = (fim.tv_sec + fim.tv_usec/1000000.0) - (inicio.tv_sec + inicio.tv_usec/1000000.0);
    printf("tempo: %f\n", total);

	//fim = clock();
	//Fim da contagem
	//total = (fim - inicio);
	//seconds = (((long double)total) / CLOCKS_PER_SEC);
	//tudo += seconds;
    tudo += total;
	//Escreve os resultados na saida padrão
	//cvShowImage("filter",aux);
		
	printf("\n");
	
	
}

unsigned char* addPadding(unsigned char* img, int size, float value, int h, int w){

	int i,j;
	unsigned char* padded;
	
	padded = (unsigned char*)malloc( ((h+4)*(w+4))*sizeof(unsigned char));
	if(padded == NULL)
		exit(1);
	
	for(i = 0; i < (h + (size*2)); i++){
		for(j = 0; j < (w + (size*2)); j++){
			
			if(i < size || j < size || i >= h || j >= w)
				padded[i*(w) + j] = value;
			else
				padded[i*(w) + j] = img[i*(w) + j];			
		}
	}
	return padded;

	}
